#splits pdf's 
#pip install pdf
#pip install pypdf2
#./python splitpdf.py file.pdf, [step]
#would be awesome to have this as an executable so that you would be able to drag and drop the pdf on the exe but.. 

from sys import argv
import py2exe
from PyPDF2 import PdfFileWriter, PdfFileReader
#print (argv[0])
if(len(argv) > 2):
    step = int(argv[2])
else:
    step = 1
    
inputpdf = PdfFileReader(open("./"+argv[1],"rb"))

for i in range(0,   inputpdf.numPages, step):
    name = inputpdf.getPage(i).extractText().split()[0]
    #name = page.
    output = PdfFileWriter()
    for j in range(step):
        output.addPage(inputpdf.getPage(i+j))
    with open(argv[1].rstrip(".pdf")+"-"+ name +".pdf", "wb") as outputStream:
        output.write(outputStream)
